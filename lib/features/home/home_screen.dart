import 'package:flutter/material.dart';
import 'package:flutter_base/features/counter/widgets/counter.dart';
import 'package:flutter_base/features/posts/models/post_model.dart';
import 'package:flutter_base/models/example_model.dart';
import 'package:flutter_base/repositories/repos.dart';
import 'package:flutter_base/repositories/test.dart';
import 'package:flutter_base/utils/extensions/build_context.dart';
import 'package:flutter_base/widgets/my_futurebuilder.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bool isTested = Test.of(context)!.isTested;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: Column(
          children: [
            const Text('Content'),
            Text('isTested = $isTested'),
            Consumer<Repos>(
              builder: (_, services, __) {
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(context.paddingScheme.p3),
                      child: Counter(),
                    ),
                    MyFutureBuilder<ExampleModel>(
                      future: services.external.fetchDummy(),
                      builder: (exampleModel) => Text(
                        'externalService.fetchDummy() = ${exampleModel.name}',
                      ),
                    ),
                    SizedBox(height: context.paddingScheme.p2),
                    MyFutureBuilder<List<PostModel>>(
                      future: services.posts.fetchPosts(),
                      builder: (exampleModel) => Text(
                        'number of fetched posts = ${exampleModel.length}',
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/placeholder');
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
