import 'package:flutter/material.dart';

abstract class CounterProvider extends ChangeNotifier {
  int get count;

  void increment();

  void decrement();
}

class CounterProviderImpl extends ChangeNotifier implements CounterProvider {
  @override
  int count;

  CounterProviderImpl({required this.count});

  @override
  void increment() {
    count++;
    notifyListeners();
  }

  @override
  void decrement() {
    count--;
    notifyListeners();
  }
}
