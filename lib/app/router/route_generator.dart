import 'package:flutter/material.dart';
import 'package:flutter_base/features/home/home_screen.dart';
import 'package:flutter_base/utils/logger.dart';

class RouteGenerator {
  static final log = getLogger(RouteGenerator);

  static Route<dynamic> generate(RouteSettings settings) {
    final name = settings.name;
    //final args = settings.arguments;

    log.i('Opening new route "$name"');
    switch (name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case '/placeholder':
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            appBar: AppBar(
              title: const Text('Placeholder'),
              foregroundColor: Colors.black,
            ),
            body: const Placeholder(),
          ),
        );
      default:
        final String message = 'Undefined route $name';
        log.f(message);
        throw Exception(message);
    }
  }
}
