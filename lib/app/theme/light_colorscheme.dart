import 'package:flutter/material.dart';

const ColorScheme flexSchemeLight = ColorScheme(
  brightness: Brightness.light,
  // Primary
  primary: Color(0xffef6a38),
  onPrimary: Color(0xffffffff),
  primaryContainer: Color(0xffffffff),
  onPrimaryContainer: Color(0xff000000),
  // Secondary
  secondary: Color(0xffef6a38),
  onSecondary: Color(0xffffffff),
  secondaryContainer: Color(0xffffffff),
  onSecondaryContainer: Color(0xff000000),
  // Tertiary
  tertiary: Color(0xffef6a38),
  onTertiary: Color(0xffffffff),
  tertiaryContainer: Color(0xffffffff),
  onTertiaryContainer: Color(0xff000000),
  // Error
  error: Color(0xffe4201c),
  onError: Color(0xffffffff),
  errorContainer: Color(0x22e4201c), // error with alpha 34
  onErrorContainer: Color(0xff000000),
  // Surface
  background: Color(0xffffffff),
  onBackground: Color(0xff000000),
  surface: Color(0xffffffff),
  onSurface: Color(0xff000000),
  surfaceVariant: Color(0xfff3f3f3), // Light emphasis
  onSurfaceVariant: Color(0xff000000),
  // Inverse
  inverseSurface: Color(0xff000000),
  onInverseSurface: Color(0xffffffff),
  inversePrimary: Color(0xffffffff),
  // Other
  outline: Color(0xff999999), // Strong emphasis
  outlineVariant: Color(0x22000000), // Divider, Medium emphasis
  shadow: Color(0xff000000),
  scrim: Color(0xff000000),
  surfaceTint: Color(0xffef6a38),
);
