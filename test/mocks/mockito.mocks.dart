// Mocks generated by Mockito 5.4.3 from annotations
// in flutter_base/test/mocks/mockito.dart.
// Do not manually edit this file.

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i4;
import 'dart:ui' as _i8;

import 'package:flutter_base/features/counter/state/counter_provider.dart' as _i7;
import 'package:flutter_base/features/posts/models/post_model.dart' as _i5;
import 'package:flutter_base/features/posts/repositories/posts_repository.dart' as _i3;
import 'package:flutter_base/models/example_model.dart' as _i2;
import 'package:flutter_base/repositories/database/external_repository.dart' as _i6;
import 'package:mockito/mockito.dart' as _i1;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: deprecated_member_use
// ignore_for_file: deprecated_member_use_from_same_package
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types
// ignore_for_file: subtype_of_sealed_class

class _FakeExampleModel_0 extends _i1.SmartFake implements _i2.ExampleModel {
  _FakeExampleModel_0(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

/// A class which mocks [PostsRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockPostsRepository extends _i1.Mock implements _i3.PostsRepository {
  MockPostsRepository() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<List<_i5.PostModel>> fetchPosts() => (super.noSuchMethod(
        Invocation.method(
          #fetchPosts,
          [],
        ),
        returnValue: _i4.Future<List<_i5.PostModel>>.value(<_i5.PostModel>[]),
      ) as _i4.Future<List<_i5.PostModel>>);
}

/// A class which mocks [ExternalRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockExternalRepository extends _i1.Mock implements _i6.ExternalRepository {
  MockExternalRepository() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<_i2.ExampleModel> fetchDummy() => (super.noSuchMethod(
        Invocation.method(
          #fetchDummy,
          [],
        ),
        returnValue: _i4.Future<_i2.ExampleModel>.value(_FakeExampleModel_0(
          this,
          Invocation.method(
            #fetchDummy,
            [],
          ),
        )),
      ) as _i4.Future<_i2.ExampleModel>);
}

/// A class which mocks [CounterProvider].
///
/// See the documentation for Mockito's code generation for more information.
class MockCounterProvider extends _i1.Mock implements _i7.CounterProvider {
  MockCounterProvider() {
    _i1.throwOnMissingStub(this);
  }

  @override
  int get count => (super.noSuchMethod(
        Invocation.getter(#count),
        returnValue: 0,
      ) as int);

  @override
  bool get hasListeners => (super.noSuchMethod(
        Invocation.getter(#hasListeners),
        returnValue: false,
      ) as bool);

  @override
  void increment() => super.noSuchMethod(
        Invocation.method(
          #increment,
          [],
        ),
        returnValueForMissingStub: null,
      );

  @override
  void decrement() => super.noSuchMethod(
        Invocation.method(
          #decrement,
          [],
        ),
        returnValueForMissingStub: null,
      );

  @override
  void addListener(_i8.VoidCallback? listener) => super.noSuchMethod(
        Invocation.method(
          #addListener,
          [listener],
        ),
        returnValueForMissingStub: null,
      );

  @override
  void removeListener(_i8.VoidCallback? listener) => super.noSuchMethod(
        Invocation.method(
          #removeListener,
          [listener],
        ),
        returnValueForMissingStub: null,
      );

  @override
  void dispose() => super.noSuchMethod(
        Invocation.method(
          #dispose,
          [],
        ),
        returnValueForMissingStub: null,
      );

  @override
  void notifyListeners() => super.noSuchMethod(
        Invocation.method(
          #notifyListeners,
          [],
        ),
        returnValueForMissingStub: null,
      );
}
