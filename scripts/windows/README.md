Prior to running any of the powershell scripts run the following scripts in your Powershell shell:
Set-ExecutionPolicy Bypass -Scope CurrentUser

The powershell scripts are not digitally signed, so without this settings you would not be able to run it.

Any work with chocolatey package manager is recommended to do in elevated shell meaning running the powershell as administrator.
